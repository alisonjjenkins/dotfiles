#!/bin/bash

DOMAINS=(
  superdry.be
  superdry.com
  superdry.de
  superdry.dk
  superdry.es
  superdry.fi
  superdry.gr
  superdry.hk
  superdry.ie
  superdry.it
  superdry.nl
  superdry.tw
  superdrystore.ca
  superdrystore.ch
  superdrystore.cn
  superdrystore.pl
  superdrystore.se
  www.superdry.be
  www.superdry.com
  www.superdry.de
  www.superdry.dk
  www.superdry.es
  www.superdry.fi
  www.superdry.gr
  www.superdry.hk
  www.superdry.ie
  www.superdry.it
  www.superdry.nl
  www.superdry.tw
  www.superdrystore.ca
  www.superdrystore.ch
  www.superdrystore.cn
  www.superdrystore.pl
  www.superdrystore.se
)

for DOMAIN in "${DOMAINS[@]}"; do
  EXPIRY=$(echo | openssl s_client -servername "${DOMAIN}" -connect "${DOMAIN}":443 2>/dev/null | openssl x509 -noout -text | grep 'Not After : ')
  echo "${DOMAIN}"
  echo "${EXPIRY}"
done